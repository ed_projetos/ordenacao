#ifndef STATEVIEW_H
#define STATEVIEW_H

#include <iostream>
#include <string>
#include <cmath>
#include <list>

#include <SFML/Graphics.hpp>

#include <thread>
#include <memory> //unique_ptr
#include <mutex>
#include <condition_variable>

using namespace sf;
using namespace std;


class ISView{
public:
    //inicia a thread
    virtual void init() = 0;

    //envia um estado para o visualizador
    virtual void push(std::function<void()> fn) = 0;

    //espera a finalizacao da thread
    virtual void wait() = 0;

    //configura o tamanho do buffer
    virtual void setQueueSize(int size) = 0;

    //habilita a sincronizacao
    virtual void setSync(bool value) = 0;

    //habilita o autoplay
    virtual void setPlay(bool value) = 0;

    //seta a cor de fundo
    virtual void setColorBack(sf::Color color) = 0;

    //seta a cor de front
    virtual void setColorFront(sf::Color color) = 0;

    //retorna o offset da parte superior
    virtual float getOffsetUp() = 0;
};

class ColorMap
{
public:
    //retorna uma string com os caracteres que representam cores
    static inline std::string getColorList();

    //dado um char que representa uma cor, retorna essa cor
    //se o caractere for invalido, retorna a cor branca
    static sf::Color getColor(char color);

    //se a tecla for de cor valida, salva em cor a nova cor
    static void updateColor(const sf::Keyboard::Key key, char * cor);
};

class Pincel
{
public:

    Pincel(std::shared_ptr<sf::RenderWindow> window);

    //desenha uma linha entre dois pontos dada a cor e a espessura
    void drawLine(Vector2f a, Vector2f b, Color color = sf::Color::Black, float espessura = 1);
    //desenha uma linha sem espessura
    void drawSimpleLine(Vector2f a, Vector2f b, Color color = sf::Color::White);
    //escreve um texto
    void drawText(Vector2f pos, string texto, Color color = sf::Color::White);
    //pega a largura do texto em pixels
    float getTextSize(string texto);
    //muda o tamanho da fonte
    void setTextSize(int value);

private:
    int textSize{20};
    std::shared_ptr<sf::RenderWindow> _window;
    sf::Font _font;
    sf::Text _text;
    const string _path = "/usr/share/fonts/truetype/droid/DroidSansMono.ttf";
};

class StateView : public ISView{
public:
    StateView(std::shared_ptr<sf::RenderWindow> window);

    virtual void init();

    virtual void push(std::function<void()> fn);

    virtual void wait();

    virtual void setQueueSize(int size);

    virtual void setSync(bool value);

    virtual void setPlay(bool value);

    virtual float getOffsetUp();

    virtual void setColorBack(sf::Color color);

    virtual void setColorFront(sf::Color color);

protected:
    std::shared_ptr<sf::RenderWindow> _janela;
    Pincel _pincel;

private:

    //metodo chamado no disparar da thread
    virtual void run();

    //remove estados antigos do buffer
    void removeFrontBuffer();

    //processa a entrada do usuario
    void processInput();
    //desenha o reloginho
    void drawClock();
    //move-se no vetor de estados
    void walk(int steps);
    //mostra os comandos na parte superior da tela
    void printComandos(int max);

    //######################################

    std::unique_ptr<std::thread> _thread;
    sf::Clock _clock;
    int _jump;
    std::list< std::function<void()> >::iterator _it;
    std::list< std::function<void()> > _vet;
    int _buffer; //maximo a frente e atras
    bool _possible_new_state = false;

   //######################################
    bool _autoplay{false};

    std::mutex _mutex_sync;
    bool _ready{false};
    bool _sync; //se nao _sync, libera o main via interface
    std::mutex _mutex_input;
    std::condition_variable _user_waiting;

    int _indice{-1};
    int _first{-1};
    std::mutex _mutex_buffer;
    std::condition_variable _buffer_full;
    int _buffer_size{1000};

    sf::Color _color_back{sf::Color::Black};
    sf::Color _color_front{sf::Color::White};
};



#endif // SLAVE_H
