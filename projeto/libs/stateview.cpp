#include "stateview.h"

StateView::StateView(std::shared_ptr<RenderWindow> window):
    _pincel(window)
{
    _sync = true;
    _janela = window;
    _it = _vet.begin();
    _jump = 1;
    _pincel.setTextSize(16);
}

void StateView::init(){
    _thread = move(unique_ptr<thread>(new std::thread(&StateView::run, this)));
}

void StateView::push(std::function<void ()> fn){
    {
        std::unique_lock<std::mutex> lock(_mutex_buffer);
        _buffer_full.wait(lock, [&]{
            return _indice - _first + _buffer_size >= (int)_vet.size();
        });
    }

    _vet.push_back(fn);

    if(_sync){
        _ready = false;
        std::unique_lock<std::mutex> lk(_mutex_input);
        _user_waiting.wait(lk, [&]{return _ready;});
    }
}

void StateView::wait(){
    _thread->join();
}

void StateView::setQueueSize(int size){
    _buffer = size/2;
}

void StateView::setSync(bool value){
    _sync = value;
}

void StateView::setPlay(bool value){
    _autoplay = value;
}

float StateView::getOffsetUp(){
    return 40;
}

void StateView::setColorBack(Color color){
    _color_back = color;
}

void StateView::setColorFront(Color color){
    _color_front = color;
}

void StateView::run(){
    int tam = 0;

    while(_janela->isOpen()){
        tam = _vet.size();
        if(_possible_new_state){
            walk(100000000);//vai pro fim
            _possible_new_state = false;
        }
        if(_autoplay)
            walk(_jump * _jump);
        processInput();
        removeFrontBuffer();

        _janela->clear(_color_back);
        printComandos(tam - 1);

        if(tam != 0){
            if(_indice == -1){
                _first = 0;
                _it = _vet.begin();
                _indice = 0;
            }
            (*_it)();//chama a funcao de desenho
        }
        this->drawClock();
        _janela->display();

    }
}

void StateView::removeFrontBuffer(){
    if(_indice > _first + _buffer_size){
        int dif = _indice - _first - _buffer_size;
        for(int i = 0; i < dif; i++){
            _vet.pop_front();
        }
        _first += dif;
    }
}

void StateView::processInput(){
    sf::Event evt;
    while(_janela->pollEvent(evt)){
        if(evt.type == sf::Event::Closed){
            _janela->close();
            _sync = false;
            _ready = true;
            _user_waiting.notify_all();
        }else if (evt.type == sf::Event::Resized){
            _janela->setView(sf::View(sf::FloatRect(0, 0, evt.size.width, evt.size.height)));
        }else if(evt.type == sf::Event::KeyPressed){
            if(evt.key.code == sf::Keyboard::Right){walk(1);}
            else if(evt.key.code == sf::Keyboard::Left){walk(-1);}
            else if(evt.key.code == sf::Keyboard::A){_autoplay = !_autoplay;}
            else if(evt.key.code == sf::Keyboard::S){
                if(_sync){
                    _sync = false;
                    //desbloqueia a main se travada
                    _ready = true;
                    _user_waiting.notify_one();
                }else{
                    _sync = true;
                }
            }
            else if(evt.key.code == sf::Keyboard::Equal){_jump++;}
            else if(evt.key.code == sf::Keyboard::Dash){
                if(_jump > 1)
                    _jump--;
            }
        }
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){walk(_jump * _jump);}
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){walk(-(_jump * _jump));}
}

void StateView::drawClock(){
    sf::CircleShape circ(3);
    circ.setFillColor(sf::Color::Red);
    int pos = _clock.getElapsedTime().asMilliseconds() / 200 % 15;
    circ.setPosition(pos * 2, 5);
    _janela->draw(circ);
}

void StateView::walk(int steps){
    int tam = _vet.size();
    if(steps > 0){
        //unlock user
        if(_sync and _it == std::prev(_vet.end())){
            _ready = true;
            _user_waiting.notify_one();
            _possible_new_state = true;
        }//move steps
        else if(_indice + steps <= tam - 1 + _first){
            _indice += steps;
            std::advance(_it, steps);
        }//move to end
        else{
            _indice = tam - 1 + _first;
            _it = std::prev(_vet.end());
        }
        _buffer_full.notify_one();
    }else{
        //move backward
        if(_indice + steps >= _first){
            _indice += steps; //steps eh negativo
            std::advance(_it, steps);//steps eh negativo
        }//move to begin
        else{
            _indice = _first;
            _it = _vet.begin();
        }
    }
}

void StateView::printComandos(int max){
    (void) max;
    string title_left = "   Jump | First | Indice | Last ";

    char estado[200];
    sprintf(estado,     "  %5d | %5d | %5d  | %5d" ,
            _jump * _jump, _first, _indice, (int)_vet.size() + _first - 1);

    _pincel.drawText(Vector2f(0, 0), title_left, _color_front);
    _pincel.drawText(Vector2f(0, 20), estado, _color_front);

    string title_right = " Step  Move | Jump Size |  Jump   | Sync | Autoplay ";
    string teclas =      " Left/Right |     -=    | Up/Down |  S   |     A    ";

    string syncOp =      "                                      ##            ";
    string autoOp =      "                                                ##  ";

    auto dim = _janela->getSize();
    float width = _pincel.getTextSize(title_right);

    _pincel.drawText(Vector2f(dim.x - width - 10, 0), title_right, _color_front);
    _pincel.drawText(Vector2f(dim.x - width - 10, 15), teclas, _color_front);
    if(_sync)
        _pincel.drawText(Vector2f(dim.x - width - 10, 15), syncOp, sf::Color::Green);
    else
        _pincel.drawText(Vector2f(dim.x - width - 10, 15), syncOp, sf::Color::Red);
    if(_autoplay)
        _pincel.drawText(Vector2f(dim.x - width - 10, 15), autoOp, sf::Color::Green);
    else
        _pincel.drawText(Vector2f(dim.x - width - 10, 15), autoOp, sf::Color::Red);

    _pincel.drawText(Vector2f(dim.x - width - 10, 15), teclas, _color_front);

    _pincel.drawLine(Vector2f(0, 40), Vector2f(dim.x, 40), _color_front, 1);
}


Pincel::Pincel(std::shared_ptr<RenderWindow> window)
{
    this->_window = window;
    if(!_font.loadFromFile(_path))
        cerr << "font " << _path << " nao encontrada" << endl;

    _text.setFont(_font);
}

void Pincel::drawLine(Vector2f a, Vector2f b, Color color, float espessura){
    RectangleShape rect;
    rect.setOutlineThickness(espessura);
    rect.setOutlineColor(color);
    rect.setPosition(a.x, a.y);

    float dist = sqrt((b.x - a.x)*(b.x - a.x) + (b.y - a.y) * (b.y - a.y));
    rect.setSize(Vector2f(dist, 0));

    auto sub = a - b;
    if(sub.x == 0)
        rect.setRotation(-90);
    else
        rect.setRotation(atan(sub.y/sub.x)*(180/M_PI));

    _window->draw(rect);
}

void Pincel::drawSimpleLine(Vector2f a, Vector2f b, Color color){
    sf::Vertex line[2] =
    {
        sf::Vertex(a, color),
        sf::Vertex(b, color)
    };
    _window->draw(line , 2, sf::Lines);
}

void Pincel::drawText(Vector2f pos, string texto, Color color){
    this->_text.setColor(color);
    this->_text.setPosition(pos);
    this->_text.setCharacterSize(textSize);
    this->_text.setString(texto);
    _window->draw(this->_text);
}

float Pincel::getTextSize(string texto){
    this->_text.setCharacterSize(textSize);
    this->_text.setString(texto);
    return this->_text.getLocalBounds().width;
}

void Pincel::setTextSize(int value){
    textSize = value;
}


string ColorMap::getColorList(){
    return "rgbwkycm";
}

Color ColorMap::getColor(char color){
    if(color == 'k')
        return sf::Color::Black;
    else if(color == 'w')
        return sf::Color::White;
    else if(color == 'r')
        return sf::Color::Red;
    else if(color == 'g')
        return sf::Color::Green;
    else if(color == 'b')
        return sf::Color::Blue;
    else if(color == 'y')
        return sf::Color::Yellow;
    else if(color == 'm')
        return sf::Color::Magenta;
    else if(color == 'c')
        return sf::Color::Cyan;
    return sf::Color::White;
}

void ColorMap::updateColor(const Keyboard::Key key, char *cor){
    if (key == sf::Keyboard::K)
        *cor = 'k';
    else if(key == sf::Keyboard::W)
        *cor = 'w';
    else if(key == sf::Keyboard::R)
        *cor = 'r';
    else if(key == sf::Keyboard::G)
        *cor = 'g';
    else if(key == sf::Keyboard::B)
        *cor = 'b';
    else if(key == sf::Keyboard::C)
        *cor = 'c';
    else if(key == sf::Keyboard::Y)
        *cor = 'y';
    else if(key == sf::Keyboard::M)
        *cor = 'm';
}
