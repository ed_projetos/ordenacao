#ifndef SORTVIEW_H
#define SORTVIEW_H

#include <SFML/Graphics.hpp>
#include "libs/stateview.h"
#include <queue>
#include <sstream>

//Implementando com composicao
class SortView{

public:
    SortView():
        SortView(std::shared_ptr<sf::RenderWindow>
                 (new RenderWindow(VideoMode(1000, 800), "Janelinha")))
    {}

    SortView(std::shared_ptr<sf::RenderWindow> janela):
        _janela(janela),
        _sview(janela),
        _pincel(janela)
    {
        _begin = 0;
        _end = 0;
        _voption = BAR;
        _sview.setSync(true);
        _sview.init();
    }

    void setHighlighted(int begin, int end){
        if(begin <= end) {
            _begin = begin;
            _end = end;
        }
    }

    void setBarView(){
        _voption = BAR;
    }

    void setDotView(){
        _voption = DOT;
    }

    void push(const vector<int> &vet, vector<unsigned int> pos = {}, string cores = ""){
        //vector<int> pos(il);
        auto f = bind(&SortView::pintura_individual,this, vet, pos, cores, _begin, _end);
        _sview.push(f);
    }

    void push(const vector<int> &vet, unsigned int begin, unsigned int end, char cor = '\0'){
        //vector<int> pos(il);
        auto f = bind(&SortView::pintura_intervalo,this, vet, begin, end, cor, _begin, _end);
        _sview.push(f);
    }

    void wait(){
        _sview.wait();
    }

private:
    std::shared_ptr<sf::RenderWindow> _janela;
    StateView _sview;
    Pincel _pincel;
    int _begin;
    int _end;
    enum ViewOption{BAR, DOT};
    ViewOption _voption;

    void pintura_individual(const vector<int> &vet, vector<unsigned int> pos, string cores, int vbeg, int vend){
        //vector<int> vet(il);
        auto dim = _janela->getSize();
        int offleft = 50;
        int offdown = 50;

        unsigned int tam = vet.size();
        float xfator = (dim.x - 100) / (float)tam;
        for(unsigned int x = 0; x < tam; x++){
            Color cor_linha = Color::White;
            for(unsigned int k = 0; k < pos.size(); k++){
                if(x == pos[k]){
                    cor_linha = ColorMap::getColor(cores[k]);
                }
            }

            if(_voption == BAR){
                _pincel.drawLine(Vector2f(xfator * x + offleft, dim.y - offdown),
                             Vector2f(xfator * x + offleft, dim.y - offdown - vet[x]/3),
                                   cor_linha, 2);
            }else{
                _pincel.drawLine(Vector2f(xfator * x + offleft, dim.y - vet[x] + 5 - offdown),
                             Vector2f(xfator * x + offleft, dim.y - vet[x] - offdown),
                                   cor_linha, 2);
            }
        }
        if(pos.size() > 0)
            _pincel.drawLine(Vector2f(xfator * vbeg - 3 + offleft, dim.y - offdown + 5),
                             Vector2f(xfator * vend + 3   + offleft, dim.y - offdown + 5),
                                   Color::Yellow, 2);

     }

    void pintura_intervalo(const vector<int> &vet, unsigned int inicio, unsigned int fim, char cor, int vbeg, int vend){
        //vector<int> vet(il);
        auto dim = _janela->getSize();
        int offleft = 50;
        int offdown = 50;

        unsigned int tam = vet.size();
        float xfator = (dim.x - 100) / (float)tam;

        Color cor_destaque = ColorMap::getColor(cor);

        for(unsigned int x = 0; x < tam; x++){
            if(_voption == BAR){
                _pincel.drawLine(Vector2f(xfator * x + offleft, dim.y - offdown),
                             Vector2f(xfator * x + offleft, dim.y - offdown - vet[x]/3),
                                   (x >= inicio && x <= fim ? cor_destaque : Color::White), 2);
            }else{
                _pincel.drawLine(Vector2f(xfator * x + offleft, dim.y - vet[x] + 5 - offdown),
                             Vector2f(xfator * x + offleft, dim.y - vet[x] - offdown),
                                   (x >= inicio && x <= fim ? cor_destaque : Color::White), 2);
            }
        }

        if(inicio <= fim)
            _pincel.drawLine(Vector2f(xfator * vbeg - 3 + offleft, dim.y - offdown + 5),
                             Vector2f(xfator * vend + 3   + offleft, dim.y - offdown + 5),
                                   Color::Yellow, 2);

     }

};
#endif // BUCKETVIEW_H

