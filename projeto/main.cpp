
#include <iostream>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "sortview.h"
#include <SFML/Graphics.hpp>

using namespace std;

SortView sview;

vector<int> merge(vector<int> &vetor, unsigned int inicio, unsigned int fim) {
    vector<int> Z;
    unsigned int meio = (inicio+fim)/2;
    unsigned int tamanho = (fim - inicio) + 1;
    unsigned int marcadorZ, marcadorX = inicio, marcadorY = meio+1;

    Z.resize(tamanho);
    sview.setHighlighted(inicio, fim);

    for(marcadorZ = 0; marcadorZ < tamanho; marcadorZ++) {

        // Apenas para nao colorir quando algum marcador passa da regiao onde atua
        if(marcadorX > meio)
            sview.push(vetor, {marcadorX, marcadorY}, "wg");
        else if(marcadorY > fim)
            sview.push(vetor, {marcadorX, marcadorY}, "mw");
        else
            sview.push(vetor, {marcadorX, marcadorY}, "mg");

        if(marcadorY > fim || (marcadorX <= meio && vetor[marcadorX] < vetor[marcadorY])) {
            // Apenas para nao colorir quando algum marcador passa da regiao onde atua
            sview.push(vetor, {marcadorX, marcadorY}, (marcadorY > fim ? "rw" : "rg"));

            Z[marcadorZ] = vetor[marcadorX];
            marcadorX++;
        } else {
            // Apenas para nao colorir quando algum marcador passa da regiao onde atua
            sview.push(vetor, {marcadorX, marcadorY}, (marcadorX > meio ? "wr" : "mr"));

            Z[marcadorZ] = vetor[marcadorY];
            marcadorY++;
        }

    }

    return Z;
}

void merge_sort(vector<int> &vetor, unsigned int inicio, unsigned int fim) {
    if(inicio < fim) {
        unsigned int meio = (inicio+fim)/2;

        sview.setHighlighted(inicio, fim);
        sview.push(vetor, {inicio, fim}, "ww");

        merge_sort(vetor, inicio, meio);
        merge_sort(vetor, meio+1, fim);


        vector<int> Z = merge(vetor, inicio, fim);
        for(unsigned int i = 0; i < Z.size(); i++) {
            vetor[inicio+i] = Z[i];
        }

        sview.push(vetor, inicio, fim, 'b');
    }
}

int main(){
    vector<int> vetor({35, 20, 1, 40, 15, 7, 25, 12, 30, 4, 17, 27});

    // apenas para os valores ficarem mais visíveis
    for(auto i = 0u; i < vetor.size(); ++i) {
        vetor[i] *= 30;
    }

    sview.setBarView();
    sview.setHighlighted(0, vetor.size()-1);

    sview.push(vetor);

    merge_sort(vetor, 0, vetor.size()-1);
    sview.wait();

    return 0;
}

