TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -L/usr/lib -lsfml-graphics -lsfml-window -lsfml-system -lpthread
INCLUDEPATH += /usr/lib/
CONFIG += c++11

SOURCES += \
    libs/stateview.cpp \
    main.cpp

HEADERS += \
    sortview.h \
    libs/stateview.h

